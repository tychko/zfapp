<?php
namespace CurrentTime\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
class CurrentTimeController extends AbstractActionController
{
    public function currentTimeAction()
    {
    $view = new ViewModel();
    $view->setTemplate('CurrentTime/current-time');
    return $view;
    }
}